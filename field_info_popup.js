(function($) {
  Drupal.behaviors.fieldInfoPopup = {
    attach: function(context, settings) {
      $('.info_popup').click(function(){
        var id = $(this).attr('data-id');
        $('#'+id).dialog({ modal: true });
      });
    }
  };
})(jQuery);
